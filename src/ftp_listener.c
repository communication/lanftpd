/*
 * Copyright 2000, 2001 Shane Kerr.  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met: 

    1.  Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer. 

    2.  Redistributions in binary form must reproduce the above
	copyright notice, this list of conditions and the following
	disclaimer in the documentation and/or other materials provided
	with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. 

 
This is the code that waits for client connections and creates the
threads that handle them.  When ftp_listener_init() is called, it binds
to the appropriate socket and sets up the other values for the
structure.  Then, when ftp_listener_start() is called, a thread is
started dedicatd to accepting connections.

This thread waits for input on two file descriptors.  If input arrives
on the socket, then it accepts the connection and creates a thread to
handle it.  If input arrives on the shutdown_request pipe, then the
thread terminates.  This is how ftp_listener_stop() signals the listener
to end.

*/

#include <config.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include <limits.h>
#include <syslog.h>
#include <stdio.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <fcntl.h>

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include "daemon_assert.h"
#include "telnet_session.h"
#include "ftp_session.h"
#include "ftp_listener.h"
#include "watchdog.h"
#include "af_portability.h"


/* maximum number of consecutive errors in accept()
   before we terminate listener                     */
#define MAX_ACCEPT_ERROR 10

/* buffer to hold address string */
#define ADDR_BUF_LEN 100

/* information for a specific connection */
typedef struct connection_info {
    ftp_listener_t *ftp_listener;
    telnet_session_t telnet_session;
    ftp_session_t ftp_session;
    watched_t watched;
    
    struct connection_info *next;
} connection_info_t;

/* prototypes */
static int invariant(const ftp_listener_t *f);
static void *connection_acceptor(ftp_listener_t *f);
static void addr_to_string(const struct sockaddr *s, char *addr);
static void *connection_handler(connection_info_t *info);
static void connection_handler_cleanup(connection_info_t *info);

/* initialize an FTP listener */
int ftp_listener_init(ftp_listener_t *f, 
                      char *address, 
                      int port, 
                      int max_connections,
                      int inactivity_timeout,
                      error_t *err)
{
#ifdef INET6
    struct sockaddr_storage sock_addr;
    int gai_err;
    struct addrinfo hints;
    struct addrinfo *res;
    char buf[ADDR_BUF_LEN+1];
    int ret;
#else 
    struct sockaddr_in sock_addr;
    char *addr_str;
#endif
    int fd;
    int flags;
    int pipefds[2];
    int reuseaddr;
    char dir[PATH_MAX+1];

    daemon_assert(f != NULL);
    daemon_assert(port >= 0);
    daemon_assert(port < 65536);
    daemon_assert(max_connections > 0);
    daemon_assert(err != NULL);

    /* get our current directory */
    if (getcwd(dir, sizeof(dir)) == NULL) {
        error_init(err, errno, "error getting current directory; %s",
                   strerror(errno));
        return 0;
    }

    /* set up our socket address */
    memset(&sock_addr, 0, sizeof(sock_addr));

#ifdef INET6
    memset(&hints, 0, sizeof(hints));
	 
    /* with AF_UNSPEC we can handle even hostnames */
    hints.ai_family    = (address == NULL ? AF_INET6 : AF_UNSPEC);
    hints.ai_flags     = AI_PASSIVE;
    hints.ai_socktype  = SOCK_STREAM;

    gai_err = getaddrinfo(address, "21", &hints, &res);							        
    if (gai_err != 0) {
	if (gai_err < 0) gai_err = -gai_err;
        error_init(err, gai_err, "error parsing server socket address; %s",
		gai_strerror(gai_err));
	return 0;
    }
	
    memcpy(&sock_addr, res->ai_addr, res->ai_addrlen);
    freeaddrinfo(res);
#else
    if (address == NULL) {
        sock_addr.sin_family = AF_INET;
        sock_addr.sin_addr.s_addr = INADDR_ANY;
    } else {
	struct hostent *hp;
		
	hp = gethostbyname(address);
	if(hp == NULL) {
		error_init(err, h_errno, "error with gethostbyname");
		return 0;
	}
		
	daemon_assert(hp->h_length <= sizeof(sock_addr));
	memcpy(&sock_addr, hp->h_addr, hp->h_length);
    }
#endif
    /* setup non-default port */
    if (port != 0) {			        
	sa_family_t family = ((struct sockaddr *)&sock_addr)->sa_family;
	switch (family) {
#ifdef INET6
	case AF_INET6:
		((struct sockaddr_in6*)&sock_addr)->sin6_port = htons(port);
		break;
#endif
	case AF_INET:
		((struct sockaddr_in*)&sock_addr)->sin_port = htons(port);
		break;
	default:
		/* handle error */
		error_init(err, 1, "unknown adderess family");
		return 0;
	}
    }
	
#ifdef INET6
    ret = getnameinfo((struct sockaddr *)(&sock_addr), sizeof(sock_addr), buf, sizeof(buf), 
					NULL, 0, NI_NUMERICHOST);
    if (ret != 0) {
        error_init(err, errno, "error converting server address to ASCII; %s", 
                   strerror(errno));
        return 0;
    }
    
    /* Put some information in syslog */
    syslog(LOG_INFO, "Binding interface '%s', port %d, max clients %d\n", buf, 
	sockaddr_port((struct sockaddr *)&sock_addr, sizeof(sock_addr)), max_connections);    
#else 
    /* this should be thread-safe, as glibc texinfo documentation states: 
     * 
     *     In multi-threaded programs each thread has an own
     *     statically-allocated buffer (for inet_ntoa).  But still 
     *     subsequent calls of `inet_ntoa' in the same thread will 
     *     overwrite the result of the last call.
     */
    addr_str = inet_ntoa(sock_addr.sin_addr);

    /* Put some information in syslog */
    syslog(LOG_INFO, "Binding interface '%s', port %d, max clients %d\n", 
        addr_str,sockaddr_port((struct sockaddr*)&sock_addr,sizeof(sock_addr)),
	max_connections);
#endif
    

    /* okay, finally do some socket manipulation */
    fd = socket(((struct sockaddr *)&sock_addr)->sa_family, SOCK_STREAM, 0);
    if (fd == -1) {
        error_init(err, errno, "error creating socket; %s", strerror(errno));
        return 0;
    }

    reuseaddr = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (void *)&reuseaddr, 
                   sizeof(int)) !=0) 
    {
        close(fd);
        error_init(err, errno, "error setting socket to reuse address; %s", 
                   strerror(errno));
        return 0;
    }

    if (bind(fd, (struct sockaddr *)&sock_addr, sizeof(sock_addr)) != 0) 
    {
        close(fd);
        error_init(err, errno, "error binding address; %s", strerror(errno));
        return 0;
    }

    if (listen(fd, SOMAXCONN) != 0) {
        close(fd);
        error_init(err, errno, "error setting socket to listen; %s", 
                   strerror(errno));
        return 0;
    }

    /* prevent socket from blocking on accept() */
    flags = fcntl(fd, F_GETFL);
    if (flags == -1) {
        close(fd);
        error_init(err, errno, "error getting flags on socket; %s", 
                   strerror(errno));
        return 0;
    }
    if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) != 0) {
        close(fd);
        error_init(err, errno, "error setting socket to non-blocking; %s", 
                   strerror(errno));
        return 0;
    }

    /* create a pipe to wake up our listening thread */
    if (pipe(pipefds) != 0) {
        close(fd);
        error_init(err, errno, "error creating pipe for internal use; %s", 
                   strerror(errno));
        return 0;
    }

    /* now load the values into the structure, since we can't fail from
       here */
    f->fd = fd;
    f->max_connections = max_connections;
    f->num_connections = 0;
    f->inactivity_timeout = inactivity_timeout;
    pthread_mutex_init(&f->mutex, NULL);

    daemon_assert(strlen(dir) < sizeof(f->dir));
    strcpy(f->dir, dir);
    f->listener_running = 0;

    f->shutdown_request_send_fd = pipefds[1];
    f->shutdown_request_recv_fd = pipefds[0];
    pthread_cond_init(&f->shutdown_cond, NULL);

    daemon_assert(invariant(f));
    return 1;
}

/* receive connections */
int ftp_listener_start(ftp_listener_t *f, error_t *err)
{
    pthread_t thread_id;
    int ret_val;
    int error_code;

    daemon_assert(invariant(f));
    daemon_assert(err != NULL);

    syslog(LOG_WARNING, "in ftp_listener_start\n");

    error_code = pthread_create(&thread_id, 
                                NULL, 
                                (void *(*)())connection_acceptor, 
                                f);

    if (error_code == 0) {
        f->listener_running = 1;
        f->listener_thread = thread_id;
        ret_val = 1;
    } else {
        error_init(err, error_code, "unable to create thread");
        ret_val = 0;
    }

    daemon_assert(invariant(f));

    return ret_val;
}


#ifndef NDEBUG
static int invariant(const ftp_listener_t *f) 
{
    int dir_len;

    if (f == NULL) {
        return 0;
    }
    if (f->fd < 0) {
        return 0;
    }
    if (f->max_connections <= 0) {
        return 0;
    }
    if ((f->num_connections < 0) || (f->num_connections > f->max_connections)) {
        return 0;
    }
    dir_len = strlen(f->dir);
    if ((dir_len <= 0) || (dir_len > PATH_MAX)) {
        return 0;
    }
    if (f->shutdown_request_send_fd < 0) {
        return 0;
    }
    if (f->shutdown_request_recv_fd < 0) {
        return 0;
    }
    return 1;
}
#endif /* NDEBUG */

/* handle incoming connections */
static void *connection_acceptor(ftp_listener_t *f)
{
    error_t err;
    int num_error;

    int fd;
    int tcp_nodelay;
#ifdef INET6
    struct sockaddr_storage client_addr;
    struct sockaddr_storage server_addr;
#else 
    struct sockaddr_in client_addr;
    struct sockaddr_in server_addr;
#endif
    unsigned addr_len;
    
    connection_info_t *info;
    pthread_t thread_id;
    int error_code;

    fd_set readfds;

    daemon_assert(invariant(f));

    syslog(LOG_WARNING, "in connection_acceptor\n");
    
    if (!watchdog_init(&f->watchdog, f->inactivity_timeout, &err)) {
        syslog(LOG_ERR, "Error initializing watchdog thread; %s", 
            error_get_desc(&err));
        return NULL;
    }

    num_error = 0;
    for (;;) {

         /* wait for something to happen */
         FD_ZERO(&readfds);
         FD_SET(f->fd, &readfds);
         FD_SET(f->shutdown_request_recv_fd, &readfds);
         select(FD_SETSIZE, &readfds, NULL, NULL, NULL);

         /* if data arrived on our pipe, we've been asked to exit */
         if (FD_ISSET(f->shutdown_request_recv_fd, &readfds)) {
             close(f->fd);
             syslog(LOG_INFO, "listener no longer accepting connections");
             pthread_exit(NULL);
         }

         /* otherwise accept our pending connection (if any) */
         addr_len = sizeof(client_addr);
         fd = accept(f->fd, (struct sockaddr *)&client_addr, &addr_len);
         if (fd >= 0) {
             
    	     syslog(LOG_WARNING, "in connection_acceptor loop\n");

             tcp_nodelay = 1;
             if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (void *)&tcp_nodelay,
                 sizeof(int)) != 0)
             {
                 syslog(LOG_ERR,
                   "error in setsockopt(), FTP server dropping connection; %s",
                   strerror(errno));
                 close(fd);
                 continue;
             }

             addr_len = sizeof(server_addr);
             if (getsockname(fd, (struct sockaddr *)&server_addr, 
                 &addr_len) == -1) 
             {
                 syslog(LOG_ERR, 
                   "error in getsockname(), FTP server dropping connection; %s",
                   strerror(errno));
                 close(fd);
                 continue;
             }

             info = (connection_info_t *)malloc(sizeof(connection_info_t));
             if (info == NULL) {
                 syslog(LOG_CRIT, 
                     "out of memory, FTP server dropping connection");
                 close(fd);
                 continue;
             }
             info->ftp_listener = f;

             telnet_session_init(&info->telnet_session, fd, fd);

             syslog(LOG_WARNING, "about to call ftp_session_init\n");

             if (!ftp_session_init(&info->ftp_session, 
                                   (struct sockaddr *)&client_addr, 
                                   (struct sockaddr *)&server_addr, 
                                   &info->telnet_session,  
                                   f->dir, 
                                   &err)) 
             {
                 syslog(LOG_ERR, 
                     "error initializing FTP session, FTP server exiting; %s",
                     error_get_desc(&err));
                 close(fd);
                 telnet_session_destroy(&info->telnet_session);
                 free(info);
                 continue;
             }


             error_code = pthread_create(&thread_id, 
                          NULL, 
                          (void *(*)())connection_handler, 
                          info);

             if (error_code != 0) {
                 syslog(LOG_ERR, "error creating new thread; %d", error_code);
                 close(fd);
                 telnet_session_destroy(&info->telnet_session);
                 free(info);
             }

             num_error = 0;
         } else {
             if ((errno == ECONNABORTED) || (errno == ECONNRESET)) {
                 syslog(LOG_NOTICE, 
                     "interruption accepting FTP connection; %s", 
                     strerror(errno));
             } else {
                 syslog(LOG_WARNING, 
                     "error accepting FTP connection; %s", 
                     strerror(errno));
                 ++num_error;
             }
             if (num_error >= MAX_ACCEPT_ERROR) {
                 syslog(LOG_ERR, 
                   "too many consecutive errors, FTP server exiting");
                 return NULL;
             }
         }
    }
}

/* convert an address to a printable string */
/* NOT THREADSAFE - wrap with a mutex before calling! */
static char *addr2string(const struct sockaddr *s)
{
    static char addr[IP_ADDRSTRLEN+1];
    int error;
    char *ret_val;

    daemon_assert(s != NULL);

#ifdef INET6
    error = getnameinfo((struct sockaddr *)s, 
                         sizeof(struct sockaddr_storage),
                         addr, 
                         sizeof(addr), 
                         NULL,
                         0, 
                         NI_NUMERICHOST);
    if (error != 0) {
        syslog(LOG_WARNING, "getnameinfo error; %s", gai_strerror(error));
        ret_val = "Unknown IP";
    } else {
        ret_val = addr;
    }
#else
    struct sockaddr_in *s_in = (struct sockaddr_in *) s;
    ret_val = inet_ntoa(s_in->sin_addr);
#endif

    return ret_val;
}


static void *connection_handler(connection_info_t *info) 
{
    ftp_listener_t *f;
    int num_connections;
    char drop_reason[80];

    syslog(LOG_WARNING, "in connection_handler\n");
    /* for ease of use only */
    f = info->ftp_listener;

    /* don't save state for pthread_join() */
    pthread_detach(pthread_self());

    /* set up our watchdog */
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    watchdog_add_watched(&f->watchdog, &info->watched);

    /* set up our cleanup handler */
    pthread_cleanup_push((void (*)())connection_handler_cleanup, info);

    /* process global data */
    pthread_mutex_lock(&f->mutex);
    num_connections = ++f->num_connections;
    syslog(LOG_INFO, "%s port %d connection", 
        addr2string((struct sockaddr *)&info->ftp_session.client_addr), 
        sockaddr_port((struct sockaddr *)&info->ftp_session.client_addr, 
	    sizeof(info->ftp_session.client_addr)));
    pthread_mutex_unlock(&f->mutex);

    /* handle the session */
    if (num_connections <= f->max_connections) {

        syslog(LOG_WARNING, "about to call ftp_session_run\n");
        ftp_session_run(&info->ftp_session, &info->watched);

    } else {

        /* too many users */
        sprintf(drop_reason, 
          "Too many users logged in, dropping connection (%d logins maximum)", 
          f->max_connections);
        ftp_session_drop(&info->ftp_session, drop_reason);

        /* log the rejection */
        pthread_mutex_lock(&f->mutex);
        syslog(LOG_WARNING, 
          "%s port %d exceeds max users (%d), dropping connection",
          addr2string((struct sockaddr *)&info->ftp_session.client_addr), 
          sockaddr_port((struct sockaddr *)&info->ftp_session.client_addr, 
	      sizeof(info->ftp_session.client_addr)),
          num_connections);
        pthread_mutex_unlock(&f->mutex);

    }

    /* exunt (pop calls cleanup function) */
    pthread_cleanup_pop(1);

    /* return for form :) */
    return NULL;
}

/* clean up a connection */
static void connection_handler_cleanup(connection_info_t *info) 
{
    ftp_listener_t *f;

    f = info->ftp_listener;

    watchdog_remove_watched(&info->watched);

    pthread_mutex_lock(&f->mutex);

    f->num_connections--;
    pthread_cond_signal(&f->shutdown_cond);

    syslog(LOG_INFO, 
      "%s port %d disconnected", 
      addr2string((struct sockaddr *)&info->ftp_session.client_addr),
      sockaddr_port((struct sockaddr *)&info->ftp_session.client_addr, 
          sizeof(info->ftp_session.client_addr)));

    pthread_mutex_unlock(&f->mutex);

    ftp_session_destroy(&info->ftp_session);
    telnet_session_destroy(&info->telnet_session);

    free(info);
}

void ftp_listener_stop(ftp_listener_t *f)
{
    daemon_assert(invariant(f));

    /* write a byte to the listening thread - this will wake it up */
    write(f->shutdown_request_send_fd, "", 1);

    /* wait for client connections to complete */
    pthread_mutex_lock(&f->mutex);
    while (f->num_connections > 0) {
        pthread_cond_wait(&f->shutdown_cond, &f->mutex);
    }
    pthread_mutex_unlock(&f->mutex);
}

