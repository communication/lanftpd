# Copyright (C) 2007 Christian Dietrich <stettberger@dokucode.de>
#
# lanftpd - easy ftp daemon, especialy for lanparties
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

INCLUDE (CheckIncludeFiles)
INCLUDE (CheckFunctionExists)
INCLUDE (CheckCSourceCompiles)
INCLUDE (CheckLibraryExists)

set(lanftpd_SRCS  
  af_portability.c
  af_portability.h
  avahi.c
  daemon_assert.c
  daemon_assert.h
  error.c
  error.h
  file_list.c
  file_list.h
  ftp_command.c
  ftp_command.h
  ftp_listener.c
  ftp_listener.h
  ftp_session.c
  ftp_session.h
  main.c
  main.h
  telnet_session.c
  telnet_session.h
  watchdog.c
  watchdog.h)

CHECK_INCLUDE_FILES(alloca.h HAVE_ALLOCA_H)
CHECK_INCLUDE_FILES(sys/time.h HAVE_SYS_TIME_H)

set (TESTSRC "#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
int main() { if ((struct tm *) 0) return 0; }")
CHECK_C_SOURCE_COMPILES("${TESTSRC}" TIME_WITH_SYS_TIME)

SET(CMAKE_EXTRA_INCLUDE_FILES alloca.h)
CHECK_FUNCTION_EXISTS(alloca HAVE_ALLOCA)
SET(CMAKE_EXTRA_INCLUDE_FILES)

set(TESTSRC "#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
int main() {off_t offset; sendfile(0, 1, &offset); }")
CHECK_C_SOURCE_COMPILES("${TESTSRC}" HAVE_SENDFILE)
IF(${HAVE_SENDFILE})
  set(HAVE_LINUX_SENDFILE 1)
ENDIF(${HAVE_SENDFILE})

CHECK_LIBRARY_EXISTS(pthread pthread_self "" HAVE_PTHREAD)
if (${HAVE_PTHREAD}) 
  set (lanftpd_LIBRARIES ${lanftpd_LIBRARIES} pthread)
endif(${HAVE_PTHREAD})

CHECK_FUNCTION_EXISTS(gmtime_r HAVE_GMTIME_R)
CHECK_FUNCTION_EXISTS(inet_aton HAVE_INET_ATON)
CHECK_FUNCTION_EXISTS(localtime_r HAVE_LOCALTIME_R)
CHECK_FUNCTION_EXISTS(select HAVE_SELECT)
CHECK_FUNCTION_EXISTS(socket HAVE_SOCKET)

CHECK_INCLUDE_FILES(fcntl.h HAVE_FCNTL_H)
CHECK_INCLUDE_FILES(limits.h HAVE_LIMITS_H)
CHECK_INCLUDE_FILES(syslog.h HAVE_SYSLOG_H)
CHECK_INCLUDE_FILES(sys/time.h HAVE_SYS_TIME_H)
CHECK_INCLUDE_FILES(sys/types.h HAVE_SYS_TYPES_H)
CHECK_INCLUDE_FILES(unistd.h HAVE_UNISTD_H)
CHECK_INCLUDE_FILES(avahi-client/client.h HAVE_AVAHI)
IF(${NO_AVAHI})
  SET(HAVE_AVAHI 0)
ENDIF(${NO_AVAHI})

CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/config.h.cmake
               ${CMAKE_CURRENT_BINARY_DIR}/config.h)

INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

add_definitions(-DHAVE_CONFIG -D_REENTRANT -D_THREAD_SAFE -DINET6)

add_executable(lanftpd ${lanftpd_SRCS})


# Do Output
IF(NOT ${HAVE_AVAHI})
  MESSAGE("Avahi Support: No Avahi support")
ELSE(NOT ${HAVE_AVAHI})
  SET(lanftpd_LIBRARIES ${lanftpd_LIBRARIES} avahi-client avahi-common)
  MESSAGE("Avahi Support: Enabled")
ENDIF(NOT ${HAVE_AVAHI})
MESSAGE("")

target_link_libraries(lanftpd ${lanftpd_LIBRARIES})
set_target_properties(lanftpd PROPERTIES COMPILE_FLAGS "-ggdb -pthread")

INSTALL(TARGETS lanftpd DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
