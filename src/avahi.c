/* $Id: avahi-publish.c 1424 2007-04-14 22:19:52Z lennart $ */

/***
  This file is part of avahi.

  avahi is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1 of the
  License, or (at your option) any later version.

  avahi is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General
  Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with avahi; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
 ***/

#ifdef HAVE_CONFIG
#include <config.h>
#endif

#ifdef HAVE_AVAHI

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <syslog.h>
#include <errno.h>
#include <stdarg.h>


#include <avahi-common/simple-watch.h>
#include <avahi-common/error.h>
#include <avahi-common/malloc.h>
#include <avahi-common/alternative.h>
#include <avahi-client/client.h>
#include <avahi-client/publish.h>

#include "daemon_assert.h"
#include "error.h"

typedef struct Config {
  char *name, *stype;
  uint16_t port;
  AvahiAddress address;
} Config;

static AvahiSimplePoll *avahi_simple_poll = NULL;
static AvahiClient *avahi_client = NULL;
static AvahiEntryGroup *entry_group = NULL;
static Config avahi_config;

static int register_stuff(Config *avahi_config);

static void entry_group_callback(AvahiEntryGroup *g, 
                                 AvahiEntryGroupState state, 
                                 void *userdata) {
  Config *avahi_config = userdata;

  if (state == AVAHI_ENTRY_GROUP_COLLISION) {
    char *n;

    n = avahi_alternative_service_name(avahi_config->name);

    avahi_free(avahi_config->name);
    avahi_config->name = n;

    register_stuff(avahi_config);
  }
}

static int register_stuff(Config *avahi_config) {
  if (!entry_group) {
    // Generate an service name
    avahi_config->name = avahi_strdup_printf
      ("lanftp@%s", avahi_client_get_host_name_fqdn(avahi_client));

    if (!(entry_group = avahi_entry_group_new(avahi_client, 
                                              entry_group_callback, 
                                              avahi_config))) {
      syslog(LOG_ERR, "Avahi: Failed to create entry group: %s\n", 
             avahi_strerror(avahi_client_errno(avahi_client)));
      return -1;
    }
  }

  if (avahi_entry_group_add_service_strlst(entry_group, AVAHI_IF_UNSPEC, 
                                           AVAHI_PROTO_UNSPEC, 0, avahi_config->name, 
                                           avahi_config->stype, NULL, NULL, 
                                           avahi_config->port, NULL) < 0) {
    syslog(LOG_ERR, "Avahi: Failed to add service: %s\n", 
           avahi_strerror(avahi_client_errno(avahi_client)));
    return -1;
  }

  avahi_entry_group_commit(entry_group);

  return 0;
}

static void client_callback(AvahiClient *c, 
                            AvahiClientState state, 
                            AVAHI_GCC_UNUSED void * userdata) {
  Config *avahi_config = userdata;

  avahi_client = c;

  switch (state) {
  case AVAHI_CLIENT_FAILURE:
    if (avahi_client_errno(c) == AVAHI_ERR_DISCONNECTED) {
      int error;

      /* We have been disconnected, so let reconnect */
      avahi_client_free(avahi_client);
      avahi_client = NULL;
      entry_group = NULL;

      if (!(avahi_client = avahi_client_new
            (avahi_simple_poll_get(avahi_simple_poll), 
             AVAHI_CLIENT_NO_FAIL, client_callback, 
             avahi_config, &error))) {
        syslog(LOG_ERR, "Avahi: Failed to create "
               "avahi_client object: %s\n", avahi_strerror(error));
        avahi_simple_poll_quit(avahi_simple_poll);
      }

    } 
    break;

  case AVAHI_CLIENT_S_RUNNING:
    if (register_stuff(avahi_config) < 0)
      avahi_simple_poll_quit(avahi_simple_poll);
    break;

  case AVAHI_CLIENT_S_COLLISION:
  case AVAHI_CLIENT_S_REGISTERING:
    if (entry_group) {
      avahi_entry_group_free(entry_group);
      entry_group = NULL;
    }
    break;
  }
}

  int
avahi_init(int port) 
{
  int error;

  avahi_config.stype = avahi_strdup("_ftp._tcp");
  avahi_config.port = port;

  if (!(avahi_simple_poll = avahi_simple_poll_new())) {
    syslog(LOG_ERR, "Avahi: Failed to create simple poll object.\n");
    goto fail;
  }
  if (!(avahi_client = avahi_client_new(avahi_simple_poll_get(avahi_simple_poll),
                                        0, client_callback, &avahi_config, &error))) {
    syslog(LOG_ERR, "Avahi: Failed to create avahi_client object: %s\n", 
           avahi_strerror(error));
    goto fail;
  }
  return 0;

fail:
  if (avahi_client)
    avahi_client_free(avahi_client);
  if (avahi_simple_poll)
    avahi_simple_poll_free(avahi_simple_poll);

  avahi_free(avahi_config.name);
  avahi_free(avahi_config.stype);
  return -1;
}

int 
avahi_start(error_t *err) 
{
    pthread_t thread_id;
    int ret_val;
    int error_code;

    daemon_assert(err != NULL);
    daemon_assert(avahi_simple_poll);

    syslog(LOG_WARNING, "avahi start\n");

    error_code = pthread_create(&thread_id, 
                                NULL, 
                                (void *(*)())avahi_simple_poll_loop, 
                                avahi_simple_poll);
    if (error_code != 0) {
        error_init(err, error_code, "unable to create thread");
        ret_val = 0;
    }
    return ret_val;
}

#endif

