/*
 * Copyright 2000, 2001 Shane Kerr.  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met: 

    1.  Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer. 

    2.  Redistributions in binary form must reproduce the above
	copyright notice, this list of conditions and the following
	disclaimer in the documentation and/or other materials provided
	with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. 
*/

/* 
 * $Id: ftp_command.h,v 1.5 2001/03/20 00:21:11 shane Exp $
 *
 * The following comands are parsed:
 *
 * USER <SP> <username>
 * PASS <SP> <password>
 * CWD  <SP> <pathname> 
 * CDUP
 * QUIT
 * PORT <SP> <host-port>
 * LPRT <SP> <host-port-long>
 * EPRT <SP> <host-port-ext>
 * PASV
 * LPSV
 * EPSV [ <SP> <optional-number-or-all> ]
 * TYPE <SP> <type-code>
 * STRU <SP> <structure-code>
 * MODE <SP> <mode-code>
 * RETR <SP> <pathname>
 * STOR <SP> <pathname>
 * PWD
 * LIST [ <SP> <pathname> ]
 * NLST [ <SP> <pathname> ]
 * SYST
 * HELP [ <SP> <string> ]
 * NOOP
 * REST <SP> <offset>
 * MKD <SP> <pathname>
 */

#ifndef FTP_COMMAND_H
#define FTP_COMMAND_H

#include <netinet/in.h>
#include <limits.h>
#include <sys/types.h>

/* special macro for handling EPSV ALL requests */
#define EPSV_ALL (-1)

/* maximum possible number of arguments */
#define MAX_ARG 2

/* maximum string length */
#define MAX_STRING_LEN PATH_MAX

typedef struct {
    char command[5];
    int num_arg;
    union {
        char string[MAX_STRING_LEN+1];
#ifdef INET6
        struct sockaddr_storage host_port;
#else
	struct sockaddr_in host_port;
#endif
        int num;
	off_t offset;
    } arg[MAX_ARG];
} ftp_command_t;


int ftp_command_parse(const char *input, ftp_command_t *cmd);

#endif /* FTP_COMMAND_H */

