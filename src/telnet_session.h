/*
 * Copyright 2000, 2001 Shane Kerr.  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met: 

    1.  Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer. 

    2.  Redistributions in binary form must reproduce the above
	copyright notice, this list of conditions and the following
	disclaimer in the documentation and/or other materials provided
	with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. 
*/


#ifndef TELNET_SESSION_H
#define TELNET_SESSION_H

/* size of buffer */
#define BUF_LEN 2048

/* information on a telnet session */
typedef struct {
    int in_fd;
    int in_errno; 
    int in_eof; 
    int in_take;
    int in_add;
    char in_buf[BUF_LEN];
    int in_buflen;

    int in_status;

    int out_fd;
    int out_errno; 
    int out_eof; 
    int out_take;
    int out_add;
    char out_buf[BUF_LEN];
    int out_buflen;
} telnet_session_t;

/* functions */
void telnet_session_init(telnet_session_t *t, int in, int out);
int telnet_session_print(telnet_session_t *t, const char *s);
int telnet_session_println(telnet_session_t *t, const char *s);
int telnet_session_readln(telnet_session_t *t, char *buf, int buflen);
void telnet_session_destroy(telnet_session_t *t);

#endif /* TELNET_SESSION_H */

