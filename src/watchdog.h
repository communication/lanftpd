/*
 * Copyright 2000, 2001 Shane Kerr.  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met: 

    1.  Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer. 

    2.  Redistributions in binary form must reproduce the above
	copyright notice, this list of conditions and the following
	disclaimer in the documentation and/or other materials provided
	with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. 
*/

#ifndef WATCHDOG_H
#define WATCHDOG_H

#include <pthread.h>

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include "error.h"

/* each watched thread gets one of these structures */
typedef struct watched {
    /* thread to monitor */
    pthread_t watched_thread;

    /* flag whether in a watchdog list */
    int in_list;

    /* time when to cancel thread if no activity */
    time_t alarm_time;

    /* for location in doubly-linked list */
    struct watched *older;
    struct watched *newer;

    /* watchdog that this watched_t is in */
    void *watchdog;
} watched_t;

/* the watchdog keeps track of all information */
typedef struct {
    pthread_mutex_t mutex;
    int inactivity_timeout;

    /* the head and tail of our list */
    watched_t *oldest;
    watched_t *newest;
} watchdog_t;

int watchdog_init(watchdog_t *w, int inactivity_timeout, error_t *err);
void watchdog_add_watched(watchdog_t *w, watched_t *watched);
void watchdog_defer_watched(watched_t *watched);
void watchdog_remove_watched(watched_t *watched);

#endif /* WATCHDOG_H */

