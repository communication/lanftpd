/*
 * Copyright 2000, 2001 Shane Kerr.  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met: 

    1.  Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer. 

    2.  Redistributions in binary form must reproduce the above
	copyright notice, this list of conditions and the following
	disclaimer in the documentation and/or other materials provided
	with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. 
*/

/*
 * $Id: error.c,v 1.4 2001/04/18 21:40:59 shane Exp $
 *
 * This data type allows modules to cleanly return error information in a 
 * relatively clean fashion.  It only includes an error number and a
 * description string right now.  It could be modified to include a large
 * number of other data, e.g. module, file/line, timestamp.  I don't
 * need that for my program right now, so I'm going to keep it simple. 
 *
 * -Shane
 */

#include <config.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "daemon_assert.h"
#include "error.h"

static int invariant(const error_t *err);

void error_init(error_t *err, int error_code, const char *desc_fmt, ...)
{
    va_list args;

    fprintf(stderr, "error_init: %d %s", error_code, desc_fmt);
    daemon_assert(err != NULL);
    daemon_assert(error_code >= 0);
    daemon_assert(desc_fmt != NULL);

    err->error_code = error_code;
    va_start(args, desc_fmt);
    vsnprintf(err->desc, sizeof(err->desc), desc_fmt, args);
    va_end(args);

    daemon_assert(invariant(err));
}

int error_get_error_code(const error_t *err)
{
    daemon_assert(invariant(err));
    return err->error_code;
}

const char *error_get_desc(const error_t *err)
{
    daemon_assert(invariant(err));
    return err->desc;
}

#ifndef NDEBUG
static int invariant(const error_t *err)
{
    if (err == NULL) {
        return 0;
    }
    if (err->error_code < 0) {
        return 0;
    }
    if (strlen(err->desc) >= sizeof(err->desc)) {
        return 0;
    }
    return 1;
}
#endif /* NDEBUG */

