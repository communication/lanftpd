/*
 * Copyright 2000, 2001 Shane Kerr.  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met: 

    1.  Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer. 

    2.  Redistributions in binary form must reproduce the above
	copyright notice, this list of conditions and the following
	disclaimer in the documentation and/or other materials provided
	with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. 

 *
 * Restrictions:
 *  - Only stream MODE is supported.
 *  - Only "ftp" or "anonymous" accepted as a user.
 */

#ifndef FTP_SESSION_H
#define FTP_SESSION_H

#include <netinet/in.h>
#include <sys/types.h>
#include <limits.h>
#include "watchdog.h"
#include "error.h"

/* data representation types supported */
#define TYPE_ASCII  0
#define TYPE_IMAGE  1

/* file structure types supported */
#define STRU_FILE   0
#define STRU_RECORD 1

/* data path chosen */
#define DATA_PORT     0
#define DATA_PASSIVE  1

/* space required for text representation of address and port, 
   e.g. "192.168.0.1 port 1024" or 
        "2001:3333:DEAD:BEEF:0666:0013:0069:0042 port 65535" */
#define ADDRPORT_STRLEN 58

/* structure encapsulating an FTP session's information */
typedef struct {
    /* flag whether session is active */
    int session_active;
  
    /* incremented for each command */
    unsigned long command_number;

    /* options about transfer set by user */
    int data_type;
    int file_structure;

    /* offset to begin sending file from */
    off_t file_offset;
    unsigned long file_offset_command_number;

    /* flag set if client requests ESPV ALL - this prevents subsequent 
       use of PORT, PASV, LPRT, LPSV, or EPRT */
    int epsv_all_set;

    /* address of client */
#ifdef INET6
    struct sockaddr_storage client_addr;
#else
    struct sockaddr_in client_addr;
#endif
    char client_addr_str[ADDRPORT_STRLEN];

    /* address of server (including IPv4 version) */
#ifdef INET6
    struct sockaddr_storage server_addr;
    struct sockaddr_in server_ipv4_addr;
#else
    struct sockaddr_in server_addr;
#endif

    /* telnet session to encapsulate control channel logic */
    telnet_session_t *telnet_session;

    /* current working directory of this connection */
    char dir[PATH_MAX+1];

    /* data channel information, including type, 
      and client address or server port depending on type */
    int data_channel;
#ifdef INET6
    struct sockaddr_storage data_port;
#else
    struct sockaddr_in data_port;
#endif
    int server_fd;

    /* watchdog to handle timeout */
    watched_t *watched;
} ftp_session_t;

int ftp_session_init(ftp_session_t *f, 
                     const struct sockaddr *client_addr, 
                     const struct sockaddr *server_addr, 
                     telnet_session_t *t,
                     const char *dir,
                     error_t *err);
void ftp_session_drop(ftp_session_t *f, const char *reason);
void ftp_session_run(ftp_session_t *f, watched_t *watched);
void ftp_session_destroy(ftp_session_t *f);

#endif /* FTP_SESSION_H */

