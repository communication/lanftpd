#cmakedefine HAVE_ALLOCA 1
#cmakedefine HAVE_ALLOCA_H 1
#cmakedefine HAVE_SYS_TIME_H 1
#cmakedefine TIME_WITH_SYS_TIME 1
#cmakedefine HAVE_SENDFILE 1
#cmakedefine HAVE_LINUX_SENDFILE 1 
#cmakedefine HAVE_GMTIME_R 1 
#cmakedefine HAVE_SOCKET 1
#cmakedefine HAVE_SELECT 1 
#cmakedefine HAVE_LOCALTIME_R 1
#cmakedefine HAVE_INET_ATON 1
#cmakedefine HAVE_FCNTL_H 1
#cmakedefine HAVE_LIMITS_H 1
#cmakedefine HAVE_SYSLOG_H 1
#cmakedefine HAVE_SYS_TIME_H 1
#cmakedefine HAVE_SYS_TYPES_H 1
#cmakedefine HAVE_UNISTD_H 1
#cmakedefine HAVE_AVAHI 1

#define RETSIGTYPE void

#define PACKAGE "oftpd"
#define VERSION "0.1"

